<?php
    require_once('main_config.php');
    require ('model/user.class.php');
    require ('controller/user-controller.php');
    
    $error='';

    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
      if (isset($_POST['register']))
      {
        if(!isset($_POST['email']) || empty($_POST['email']))
        {
          $error='Morate ukucati Vaš email';          
        }
        elseif(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
        { 
          $error='Morate pravilno ukucati Vaš email';
        }
        else
        {
            $email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
        }

        if(!isset($_POST['password']) || empty($_POST['password']))
        {
            $error='Morate ukucati Vašu lozinku';          
        }
        elseif(strlen($_POST['password']) <= 5)
        { 
            $error='Lozinka mora da sadrži najmanje 6 karaktera';
        }
        else
        {
            $pass = $_POST['password'];
        }
        
        if (isset($email) && (isset($pass)))
        {
          $user = new UserController();

          if($user->checkIfEmailExists($email) == false)
          {
            
            $user->addNewUser($email, $pass);

          }
          else
          {
            $error='Ovaj email već postoji';
          }  
        }        
      }
      
      
    }

      //Login
      if (isset($_POST['login']))
      {
        if(!isset($_POST['email']) || empty($_POST['email']))
        {
          $error='Morate ukucati Vaš email';          
        }
        elseif(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
        { 
          $error='Morate pravilno ukucati Vaš email';
        }
        else
        {
            $email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
        }
        if(!isset($_POST['password']) || empty($_POST['password']))
        {
            $error='Morate ukucati Vašu lozinku';          
        }
        elseif(strlen($_POST['password']) <= 5)
        { 
            $error='Lozinka mora da sadrži najmanje 6 karaktera';
        }
        else
        {
            $pass = $_POST['password'];
        }
        if (isset($email) && (isset($pass)))
        {
          $UserController = new UserController();

          if($UserController->login($email, $pass) != false)
          {
            $user = new User();
            $user = $UserController->getUserByEmail($email);
            session_start();
            $_SESSION['user'] = $email;
            $url = BASE_URL.'control_panel.php';
            header('Location:'.$url);
          }
          else
          {
            $error = 'Pogrešan email ili lozinka';
          }  
        }
      }  
?>
<html>
<head>
  <title>
    Diwanee zadatak
  </title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <style>
        #login_form
        {
          display: inline-block;
          position: absolute;
        }
        label
        {
          display: block;
          position: relative;
          margin: 5 0 5px 0;
        }
        input
        {
          display: inline-block;
          margin-bottom: 5px;
        }
    </style>
</head>
<body>
  <form id="login_form" name="login" method="post" action="">
    <?php
      if($error!='')
      {
        echo $error;
      }
    ?>
    <label for="email">
      Email
    </label>
    <input type="text" name="email"/>
    <label for="password">
      Password
    </label>
    <input type="password" name="password"/>
    <br>
    <input type="submit" name="login" value="Login" />
    <input type="submit" name="register" value="Register" />
  </form>
</body>
</html>