<?php
    include_once(__DIR__ . '/../main_config.php');
    
    class UserController
    {

        function __construct()
        {
            global $host;
            global $username;
            global $password;
            global $databaseName;
            
            $this->db_host = $host;
            $this->db_username = $username;
            $this->db_password = $password;
            $this->db_databaseName = $databaseName;
                                                           
        }


        function checkIfEmailExists($email)
        {
            
            $mysqli = new mysqli($this->db_host, $this->db_username, $this->db_password, $this->db_databaseName);
            
            $query = 'SELECT * FROM users WHERE email = ?';
            $stmt = $mysqli->stmt_init();
            $stmt = $mysqli->prepare($query);
            $stmt->bind_param('s', $email);
            $stmt->execute();
            $stmt->bind_result($id, $email, $pass, $created_at);
            while ($stmt->fetch())
            {
                $emailExists = $email;
            }
            $stmt->close();
            $mysqli->close();

            if(isset($emailExists))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        function addNewUser($email, $pass)
        {

            $mysqli = new mysqli($this->db_host, $this->db_username, $this->db_password, $this->db_databaseName);


            $created_at = date('Y-m-d H:i:s');
            $pass = password_hash($pass, PASSWORD_BCRYPT);
            $stmt = $mysqli->prepare("INSERT INTO users (email, pass, created_at) VALUES (?, ?, ?)");

            $stmt->bind_param('sss', $email, $pass, $created_at);
            $stmt->execute();

            if($stmt->affected_rows > 0)
            {
                $stmt->close();    
                $mysqli->close();
                session_start();
                $_SESSION['user'] = $email;
                $url = BASE_URL.'control_panel.php';
                header('Location:'.$url);
            }
            else
            {
                echo 'greška';
            }
        }


        function login ($email, $pass)
        {

            $mysqli = new mysqli($this->db_host, $this->db_username, $this->db_password, $this->db_databaseName);
            
            $query = 'SELECT email, pass FROM users WHERE email=? LIMIT 1';
            $stmt = $mysqli->prepare($query);
            $stmt->bind_param('s', $email);
            $stmt->execute();
            $stmt->bind_result($email, $password);

            while ($stmt->fetch())
            {
                $user_email = $email;
                $user_pass = $password;
            }
            
            $stmt->close();
            $mysqli->close();

            if(!isset($user_pass))
            {
                return false;
            }
            else
            {
                if (password_verify($pass, $user_pass))
                {
                    return true;
                }
                else
                {
                    return false;
                }    
            }
            
            
        }

        function logout ()
        {
            session_start();
            
            session_destroy();

            $url = BASE_URL;
            header('Location:'.$url);
        }

        function getAllUsers()
        {
            $mysqli = new mysqli($this->db_host, $this->db_username, $this->db_password, $this->db_databaseName);

            $query = 'SELECT id, email, created_at FROM users ORDER BY created_at DESC';
            $stmt = $mysqli->prepare($query);
            $stmt->execute();
            $stmt->bind_result($id, $email, $created_at);

            $user;

            while ($stmt->fetch())
            {
                $user = new User();
                $user->id = $id;
                $user->email = $email;           
                $user->created_at=$created_at;
                $users[] = $user;
            }

            $stmt->close();
            $mysqli->close();            

            return $users;
        }
        
        
         function getUserByEmail($email)
        {
            $mysqli = new mysqli($this->db_host, $this->db_username, $this->db_password, $this->db_databaseName);
            
            $query = 'SELECT id, email, pass, created_at FROM users WHERE email=? LIMIT 1';
            $stmt = $mysqli->prepare($query);
            $stmt->bind_param('s', $email);
            $stmt->execute();
            $stmt->bind_result($id, $email, $pass, $created_at);
            
            while ($stmt->fetch())
            {
                $user = new User();
                $user->id = $id;
                $user->email = $email;
                $user->created_at = $created_at;

                session_start();
                $_SESSION['user'] = $user->email;

                return $user;
            }

            $stmt->close();
            $mysqli->close();
        }
    }
    
?>