<?php
include_once(__DIR__ . '/main_config.php');
require ('model/user.class.php');
require ('controller/user-controller.php');
session_start();

if(!isset($_SESSION['user']))
{
     header ('Location:'.BASE_URL);
     exit;
}

echo '<p><a href="'.BASE_URL.'control_panel.php">home</a>|list all users|<a href="'.BASE_URL.'logout.php">logout</a></p>';
$UserController = new UserController();

if($UserController->getAllUsers() != false)
{
    	$users = $UserController->getAllUsers();
}

if(empty($users))
{
	echo '<p>Trenutno nema registrovanih korisnika</p>';
}
else
{
	echo '<h3>Korisnici ulogovani na sistem</h3>';
	foreach ($users as $user)
	{
		echo '<p>Korisnik: '.$user->email.' je kreiran : '.$user->created_at.'</p>';
	}
}
?>